(define-module (clambda ctypes)
  #:use-module (clambda types)
  #:export (bool char short int long uchar ushort uint ulong float double
                 numtypes bittypes inttypes))

(define-c-types)

(define numtypes (list char short int long uchar ushort
                       uint ulong float double))
(define inttypes (list char short int long uchar ushort uint ulong))
(define bittypes (list uchar ushort uint ulong))
#|
(add-derive uint   -> ulong)
(add-derive ushort -> uint)
(add-derive uchar  -> ushort)

(add-derive int   -> long)
(add-derive int   -> ulong)
(add-derive short -> int)
(add-derive short -> uint)
(add-derive char  -> short)
(add-derive char  -> ushort)

(add-derive float -> double)
|#

(define-syntax-rule (defu uc p)
  (add-validator uc
     (lambda (x)
       (and (integer? x)
            (<= x p)
            (>= x 0)))))

(defu uchar  #xff)
(defu ushort #xffff)
(defu uint   #xffffffff)
(defu ulong  #xffffffffffffffff)

(define-syntax-rule (defi char p)
  (add-validator char
    (lambda (x)
      (and (integer? x)
           (<= x p)
           (>= x (- (+ p 1)))))))

(defi char  #x7f)
(defi short #x7fff)
(defi int   #x7fffffff)
(defi long  #x7fffffffffffffff)

(add-validator double
  (lambda (x)
    (number? x)
    (or
     (integer? x)
     (inexact? x))))

(add-validator float
  (lambda (x)
    (and
     (number? x)
     (<= x 3.402823466e+38)
     (>= x 1.175494351e-38))))
