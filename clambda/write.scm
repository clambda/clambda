(define-module (clambda write)
  #:use-module (ice-9 match)
  #:use-module (clambda clang)
  #:replace (write))

(define* (write-type port t x #:optional (a? #f) (f (lambda () #f)))
  (define (! s) (lambda (t) (eq? s t)))
  (define fkn? #f)

  (match t
    (((? (! cconst)) (and t ((? (! cconst)) _)))
     (write-type port t x a? f))
    
    (((? (! cconst)) t)
     (write-type port t x #t
                 (lambda ()
                   (if a? (format port " const "))
                   (f))))
    (((? (! cref)) t)
     (write-type port t x #t
                 (lambda ()
                   (format port " * ")
                   (f))))
    
    (((? (! cstruct)) tt)     
     (format port "(struct ~a)" (procedure-name tt))
     (f))
    
    (((? (! cunion)) tt)
     (format port "(union  ~a)" (procedure-name tt))
     (f))
    
    (((? (! cfkn)) ft . a)
     (write-type port ft #f)
     (set! fkn? #t)
     (if x
         (begin
           (format port " (")
           (f)
           (format port "~a) (" x))
         (begin
           (format port " (")
           (f)
           (format port ") (")))
     (for-each (lambda (x)
                 (format port " ")
                 (write-type port x #f #t))
               a)
     (format port ")"))
    (t
     (format port "~a" (procedure-name t))
     (f)))
  
  fkn?)

(define binops
  `((,c+    . "+")
    (,c-    . "-")
    (,c*    . "*")
    (,c/    . "/")
    (,c%    . "%")
    (,cand  . "&&")
    (,cor   . "||")
    (,cband . "&")
    (,cbor  . "|")
    (,cbxor . "^")
    (,cmember . ".")
    (,c<    . "<")
    (,c>    . ">")
    (,c<=   . "<=")
    (,c>=   . ">=")
    (,c==   . "==")
    (,c!=   . "!=")
    (,c=    . "=")
    (,c+=   . "+=")
    (,c-=   . "-=")
    (,c*=   . "*=")
    (,c/=   . "c/=")))

(define (binop? x)
  (assoc x binops))

(define preops
  `((,c++     . "++")
    (,c--     . "--")
    (,cnot    . "!")
    (,cbnot   . "~")
    (,c+      - "+")
    (,c-      . "-")
    (,cderef  . "*")
    (,cref    . "&")))

(define (preops? x)
  (assoc x preops))

(define postops
  `((,c++2    . "++")
    (,c--2    . "--")))

(define (postops? x)
  (assoc x postops))


(define (write port code)
  (define (! s) (lambda (t) (eq? s t)))
  (match code
    (((? (! cbegin)) (and x ((? (! cbegin)) . _)))
     (write port x))
    
    (((? (! cif)) p x y)
     (format port "if (")
     (write port p)
     (format port ")~%")
     (write port (list cbegin x))
     (format port "~%")
     (write port (list cbegin y)))
    
    (((? (! cbegin)) . l)
     (format port "{")
     (for-each (lambda (x)
                 (write port x)
                 (format port ";~%"))
               l)
     (format port "}"))

    (((? (! c:)) t x)
     (if (write-type port t x)
         #f
         (begin
           (format port " ")
           (write port x))))
    
    (((? (! ccast)) t x)
     (format port "((")
     (write-type port t #f)
     (format port ")")
     (write port x)
     (format port ")"))

    (((? (! cvar)) t x)
     (if (write-type port t x)
         (format port ";~%")
         (format port " ~a;~%" x)))

    (((? (! ctypedef)) t tt)
     (format port "typedef ")
     (if (write-type port t tt)
         (format port ";~%")
         (format port " ~a;~%" tt)))

    (((? (! ccast)) t x)
     (format port "(")
     (write-type port t #f)
     (format port ") ")
     (write port x))

    (((? (! cdefine)) tp nm args code)
     (write-type port tp #f)
     (format port " ~a (" nm)
     (let ((b (reverse
               (let ((b (map (lambda (a) ",") args)))
                 (if (pair? b)
                     (cons "" (cdr b))
                     b)))))
       (for-each
        (lambda (a b)
          (match a
            ((t v)
             (if (write-type port t v #t)
                 (format port "~a" b)
                 (format port " ~a~a" v b)))))
        args b)
       (format port "){~%")
       (write port code)
       (format port "}~%")))
       
    (((? (! cstruct)) nm a ...)
     (format port "struct ~a { ~%" (procedure-name nm))
     (for-each
      (lambda (x)
        (write port x)
        (format port ";~%"))
      a)
     (format port "}~%"))

    (((? (! cunion)) nm a ...)
     (format port "union ~a { ~%" (procedure-name nm))
     (for-each
      (lambda (x)
        (write port x)
        (format port ";~%"))
      a)
     (format port "}~%"))
    
    (((? binop? op) x y)
     (format port "(")
     (write port x)
     (format port " ~a " (cdr (assoc op binops)))
     (write port y)
     (format port ")"))

    (((? preops? op) x)
     (format port "(~a " (cdr (assoc op preops)))
     (write port x)
     (format port ")"))

    (((? postops? op) x)
     (format port "(")
     (write port x)
     (format port "~a)" (cdr (assoc op postops))))

    (((? (! ccall)) f . l)
     (format port "(* ~a)(" f)
     (let ((r (cons "" (cdr (map (lambda (x) ", ") l)))))
       (for-each
        (lambda (r x)
          (format port "~a~a" r x))
        r l))
     (format port ")"))
    
    (((? (! label)) x)
     (format port "~a: " x))
    
    (((? (! goto))  x)
     (format port "goto ~a" x))

    (((? (! creturn)) x)
     (format port "return ")
     (write port x))

    (x
     (format port "~a" x))))





    
       
    
