(define-module (clambda clang)
  #:use-module (logic guile-log iso-prolog)
  #:export (c cbegin   cif      c:       clet     c=       c+      
              c*       c/       c%       cnot     cbnot    c++     
              c--      cderef   c+=      c-=      c*=      cref    
              c%       cand     cor      cband    cbor     cbxor   
              c<       c>       c<=      c>=      c==      c!=     
              ctrue    cfalse   ccall    label    goto     cmember
              ret      cbegin2  cstruct  cdefine  cfkn     cunion
              ccast    ctypedef cvar     c-       c/=      c++2
              c--2     creturn  cconst))

(compile-prolog-string
 "
cconst.
creturn.
cvar.
ctypedef.
ccast.
cunion.
cfkn.
cdefine.
cstruct.
cmember.
cdefine.
ret.
c.       
cbegin.  
cbegin2.  
cif.     
'c:'.      
clet.    
'c='.      
'c+'.
'c-'.      
'c*'.      
'c/'.      
'c%'.      
cnot.    
cbnot.   
'c++'.     
'c--'.
'c++2'.     
'c--2'.     
cderef.  
'c+='.     
'c-='.     
'c*='.
'c/='.     
cref.    
'c%'.      
cand.    
cor.
cband.   
cbor.
cbxor. 
'c<'.      
'c>'.      
'c<='.     
'c>='.     
'c=='.     
'c!='.     
ctrue.   
cfalse.  
ccall.    
label.   
goto.    
")
