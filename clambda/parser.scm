(define-module (clambda c-parser)
  #:use-module (logic guile-log parser)
  #:export ())



  A.2.1 Expressions
(define primary-expression
  (f-or! identifier
         constant
         string-literal
         (f-seq "(" (D expression) ")")))

(define postfix-expression
  (f-or! (f-seq (D postfix-expression) "[" (D expression) "]")
         (f-seq (D postfix-expression) "(" (D argument-expression-listopt) ")")
         (f-seq (D postfix-expression) "." identifier)
         (f-seq (D postfix-expression) "->" identifier)
         (f-seq (D postfix-expression) "++")
         (f-seq (D postfix-expression) "--")
         (f-seq "(" type-name ")" "{" initializer-list "}")
         (f-seq "(" type-name ")" "{" initializer-list "," "}")
         
(define argument-expression-list
  (f-or! (D assignment-expression)
         (f-seq (D argument-expression-list) "," (D assignment-expression))))

(define unary-operator (f-reg "&|*|+|-|~|!"))

(define  unary-expression
  (f-or! (D postfix-expression)
         (f-seq "++" (D unary-expression))
         (f-seq "--" (D unary-expression))
         (f-seq unary-operator (D cast-expression))
         (f-seq "sizeof" (D unary-expression))
         (f-seq "sizeof" "(" type-name ")")))



(define cast-expression
  (f-or! unary-expression
         (f-seq "(" type-name ")" (D cast-expression))))

(define multiplicative-expression
  (f-or! cast-expression
         (f-seq (D multiplicative-expression) "*" cast-expression)
         (f-seq (D multiplicative-expression) "/" cast-expression)
         (f-seq (D multiplicative-expression) "%" cast-expression)))

(define additive-expression
  (f-or! multiplicative-expression
         (f-seq (D additive-expression) "+" multiplicative-expression)
         (f-seq (D additive-expression) "-" multiplicative-expression)))

(define shift-expression
  (f-or! additive-expression
         (f-seq (D shift-expression) "<<" additive-expression)
         (f-seq (D shift-expression) ">>" additive-expression)))

(define relational-expression
  (f-or! shift-expression
         (f-seq (D relational-expression) "<"  shift-expression)
         (f-seq (D relational-expression) ">"  shift-expression)
         (f-seq (D relational-expression) "<=" shift-expression)
         (f-seq (D relational-expression) ">=" shift-expression)))

(define equality-expression
  (f-or! relational-expression
         (f-seq (D equality-expression) "==" relational-expression)
         (f-seq (D equality-expression) "!=" relational-expression)))

(define AND-expression
  (f-or! equality-expression
         (f-seq (D AND-expression) "&" equality-expression)))

(define exclusive-OR-expression
  (f-or! AND-expression
         (f-seq (D exclusive-OR-expression) "^" AND-expression)))

(define inclusive-OR-expression
  (f-or! exclusive-OR-expression
         (f-seq (D inclusive-OR-expression) "|" exclusive-OR-expression)))

(define logical-AND-expression
  (f-or! inclusive-OR-expression
         (f-seq (D logical-AND-expression) "&&" inclusive-OR-expression)))

(define logical-OR-expression
  (f-or! logical-AND-expression
         (f-seq (D logical-OR-expression) "||" logical-AND-expression)))

(define conditional-expression
  (f-or! logical-OR-expression
         (f-seq logical-OR-expression
                "?" (D expression) ":" (D conditional-expression))))

(define assignment-operator
  (f-or! "=" "*=" "/=" "%=" "+=" "-=" "<<=" ">>=" "&=" "^=" "|="))

(define assignment-expression
  (f-or! conditional-expression
         (f-seq unary-expression assignment-operator
                (D assignment-expression))))

(define expression
  (f-or! assignment-expression
         (f-seq (D expression) "," assignment-expression)))

(define constant-expression conditional-expression)

(define declaration
  (f-seq declaration-specifiers init-declarator-listopt ";"))

(define declaration-specifiers
  (f-or!
   (f-seq storage-class-specifier declaration-specifiersopt)
   (f-seq type-specifier          declaration-specifiersopt)
   (f-seq type-qualifier          declaration-specifiersopt)
   (f-seq function-specifier      declaration-specifiersopt)))

(define init-declarator-list
  (f-or! init-declarator
         (f-seq init-declarator-list "," init-declarator)))

(define init-declarator
  (f-or! declarator
         (f-seq declarator "=" initializer)))

(define storage-class-specifier
  (f-or! "typedef" "extern" "static" "auto" "register"))


(define type-specifier
  (f-or! "void" "char" "short" "int" "long" "float" "double" "signed"
         "unsigned" "_Bool" "_Complex"
         struct-or-union-specifier
         enum-specifier
         typedef-name))

(define struct-or-union-specifier
  (f-or!
   (f-seq struct-or-union (f-or identifier true)
          "{" struct-declaration-list "}")
   (f-seq struct-or-union identifier)))

(define struct-or-union (f-or! "struct" "union"))

(define struct-declaration-list
  (f-or! struct-declaration
         (f-seq struct-declaration-list struct-declaration)))

(define struct-declaration
  (f-seq specifier-qualifier-list struct-declarator-list ";"))

(define specifier-qualifier-list
  (f-or!
   (f-seq type-specifier (f-or specifier-qualifier-list f-true))
   (f-seq type-qualifier (f-or specifier-qualifier-list f-true))))

(define struct-declarator-list
  (f-or! struct-declarator
         (f-seq struct-declarator-list "," struct-declarator)))

(define struct-declarator
  (f-or! declarator
         (f-seq (f-or declarator f-true) ":" constant-expression)))

(define enum-specifier
  (f-or!
   (f-seq "enum" (f-or identifier f-true) "{" enumerator-list "}")
   (f-seq "enum" (f-or identifier f-true) "{" enumerator-list "," "}")
   (f-seq "enum" identifier)))

(define enumerator-list
  (f-or! enumerator
         (f-seq enumerator-list "," enumerator)))

(define enumerator
  (f-or enumeration-constant
        (f-seq enumeration-constant "=" constant-expression)))

(define type-qualifier
  (f-or! "const" "restrict" "volatile"))

(define function-specifier (f-seq "inline"))

(define declarator (f-seq pointeropt direct-declarator))
(define direct-declarator
  (f-or! identifier
         (f-seq "(" declarator ")")
         (f-seq direct-declarator "[" (f-or type-qualifier-list   f-true)
                                      (f-or assignment-expression f-true) "]")
         (f-seq direct-declarator "[" static (f-or type-qualifier-list f-true)
                                      assignment-expression  "]")
         (f-seq direct-declarator "[" type-qualifier-list static
                                      assignment-expression "]")
         (f-seq direct-declarator "[" type-qualifier-listopt "*" "]")
         (f-seq direct-declarator "(" parameter-type-list ")")
         (f-seq direct-declarator "(" (f-or identifier-list f-true) ")")))

(define pointer
  (f-or!
   (f-seq "*" (f-or type-qualifier-list f-true))
   (f-seq "*" (f-or type-qualifier-list f-true) pointer)))

(define type-qualifier-list
  (f-or! type-qualifier
         (f-seq type-qualifier-list type-qualifier)))

(define parameter-type-list
  (f-or! parameter-list
         (f-seq parameter-list "," "...")))

(define parameter-list
  (f-or! parameter-declaration
         (f-seq parameter-list "," parameter-declaration)))

(define parameter-declaration
  (f-or!
   (f-seq declaration-specifiers declarator)
   (f-seq declaration-specifiers (f-or abstract-declarator f-true))))

(define identifier-list
  (f-or!
   identifier
   (f-seq identifier-list "," identifier)))

(define type-name
  (f-seq specifier-qualifier-list (f-or abstract-declarator f-true)))

(define abstract-declarator
  (f-or! pointer
         (f-seq (f-or pointer f-true) direct-abstract-declarator)))

(define direct-abstract-declarator
  (f-or!
   (f-seq "(" abstract-declarator ")")
   (f-seq (f-or direct-abstract-declarator f-true)
          "["
          (f-or type-qualifier-list   f-true)
          (f-or assignment-expression f-true)
          "]")
   (f-seq (f-or direct-abstract-declarator f-true)
          "["
          (f-or static type-qualifier-list f-true)
          assignment-expression
          "]")
   (f-seq (f-or direct-abstract-declarator f-true)
          "["
          type-qualifier-list
          "static" assignment-expression
          "]")
   (f-seq (f-or direct-abstract-declarator f-true)  "[" "*" "]")
   (f-seq (f-or direct-abstract-declarator f-true)
          "(" (f-or parameter-type-list f-true) ")")))

(define typedef-name identifier)

(define initializer
  (f-or! assignment-expression
         (f-seq "{" initializer-list "}")
         (f-seq "{" initializer-list "," "}")))

(define initializer-list
  (f-or!
   (f-seq (f-or designation f-true) initializer)
   (f-seq initializer-list "," (f-or designation f-true) initializer)))

(define designation (f-seq designator-list "="))

(define designator-list
  (f-or! designator
         (f-seq designator-list designator)))

(define designator
  (f-or! (f-seq "[" constant-expression "]")
         (f-seq "." identifier)))


(6.8) statement:
labeled-statement
compound-statement
expression-statement
selection-statement
iteration-statement
jump-statement
(6.8.1) labeled-statement:
identifier : statement
case constant-expression : statement
default : statement
(6.8.2) compound-statement:
{ block-item-listopt }
(6.8.2) block-item-list:
block-item
block-item-list block-item
(6.8.2) block-item:
declaration
statement
(6.8.3) expression-statement:
expressionopt ;
(6.8.4) selection-statement:
if ( expression ) statement
if ( expression ) statement else statement
switch ( expression ) statement
§A.2.3 Language syntax summary 415
ISO/IEC 9899:TC3 Committee Draft — Septermber 7, 2007 WG14/N1256
(6.8.5) iteration-statement:
while ( expression ) statement
do statement while ( expression ) ;
for ( expressionopt ; expressionopt ; expressionopt ) statement
for ( declaration expressionopt ; expressionopt ) statement
(6.8.6) jump-statement:
goto identifier ;
continue ;
break ;
return expressionopt ;
A.2.4 External definitions
(6.9) translation-unit:
external-declaration
translation-unit external-declaration
(6.9) external-declaration:
function-definition
declaration
(6.9.1) function-definition:
declaration-specifiers declarator declaration-listopt compound-statement
(6.9.1) declaration-list:
declaration
declaration-list declaration





