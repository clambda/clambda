(define-module (clambda macros)
  #:use-module (clambda clang)
  #:use-module (clambda ctypes)
  #:use-module (clambda types)
  #:use-module (clambda write)
  #:use-module (ice-9 match)
  #:export (<let> <if> call <begin> <=> <not> <bnot> <++> <--> <deref> <+=>
                  <-=> <*=> <ref> <%>  <and> <or> <band> <bor> <bxor> <+> <*>
                  <c> </> <:> <->> q< q<= q>= q> q== struct & && &&& const fkn
                  with-c-symbols compiler
                  c-define c-struct c-declare))

(define port #t)

(define (pku x)
  (pk (syntax->datum x))
  x)

(define hex "(0[xX])([0-9aAbBcCdDeEfF]+)")
(define dec "(0[oObB])?([0-9]+)")
(define (mkr r) (cons (make-regexp (string-append hex r "$"))
                      (make-regexp (string-append dec r "$"))))
(define r-long   (mkr "([Ll])?"))
(define r-ulong  (mkr "(UL|ul)"))
(define r-int    (mkr "([Ii])" ))
(define r-uint   (mkr "(UI|ui)"))
(define r-short  (mkr "([Ss])" ))
(define r-ushort (mkr "(US|us)"))
(define r-char   (mkr "([Hh])" ))
(define r-uchar  (mkr "(UH|uh)"))
(define decimal
  "([0-9]+|[0-9]+\\.[0-9]*|[0-9]+\\.[0-9]+[eE][+-]?[0-9]+)(UC|uc)$")
(define (duo x) (cons x x))
(define r-double (duo (make-regexp (string-append decimal "[Ff]"))))
(define r-float  (duo (make-regexp (string-append decimal "[Dd]"))))

  
(define-syntax-rule (aif t p x y) (let ((t p)) (if t x y)))
(define (test r x)
  (let ((x (symbol->string x)))
    (aif it (or (regexp-exec (car r) x) (regexp-exec (cdr r) x))
         (let ((s1.e1 (vector-ref it 2))
               (s.e (vector-ref it 3)))
           (if (= (car s1.e1) -1)
               (string->number (substring x (car s.e) (cdr s.e)))
               (let ((hex (substring x (car s1.e1) (cdr s1.e1))))
                 (string->number (substring x (car s.e) (cdr s.e))
                                 (cond
                                  ((equal? hex "0x") 16)
                                  ((equal? hex "0b") 2)
                                  ((equal? hex "0o") 8)
                                  ((equal? hex "0X") 16)
                                  ((equal? hex "0B") 2)
                                  ((equal? hex "0O") 8))))))
                
         #f)))

(define (action y)
  (lambda (x)
    #`(list c: '() #,(datum->syntax #'float (procedure-name y)) #,x)))

(define (action-f y)
  (lambda (x)
    (list y x)))

(define (handle-val xin)
  (let ((x (syntax->datum xin)))
    (cond
     ((eq? x #t)
      #'ctrue)
     ((eq? x #f)
      #'cfalse)
     ((symbol? x)
      (cond
       ((test r-long   x) => (action long   ))
       ((test r-ulong  x) => (action ulong  ))
       ((test r-int    x) => (action int    ))
       ((test r-uint   x) => (action uint   ))
       ((test r-short  x) => (action short  ))
       ((test r-ushort x) => (action ushort ))
       ((test r-char   x) => (action char   ))
       ((test r-uchar  x) => (action uchar  ))
       ((test r-double x) => (action double ))
       ((test r-float  x) => (action float  ))
       (else xin)))
     (else
      xin))))


(define-syntax <let>
  (lambda (xx)
    (syntax-case xx ()
      ((<let> lp (x ...) code ...)
       (symbol? (syntax->datum #'lp))
       (with-syntax (((var ...) (map (lambda (x)
                                       (syntax-case x ()
                                         ((t v i) #'v)
                                         ((v i)   #'v)))
                                     #'(x ...)))
                     ((x2 ...) (map (lambda (x)
                                      (syntax-case x ()
                                        ((t v i)
                                         #`(list t v #,(handle-val #'i)))
                                        ((v i)
                                         #`(list v #,(handle-val #'i)))))
                                    #'(x ...))))
         #`(let ((lpn (gensym "lp"))
                 (var (gensym "a"))...)
             (list clet '#,(datum->syntax #'<let>
                                              (source-properties
                                               (syntax->datum xx)))

                   lpn (list (list . x2) ...)
                   (<let> ()
                          (define-syntax lp
                            (lambda (xx)
                              (syntax-case xx ()
                                ((_ . l)
                                 #`(list lpn '#,(datum->syntax
                                                 #'<let>
                                                 (source-properties
                                                  (syntax->datum xx)))
                                         . l)))))
                          code ...)))))
            
      ((<let> () code ...)
       (let ((pre.code (let lp ((x #'(code ...)) (pre '()))
                         (match x
                           ((x . l)
                            (syntax-case x (define-syntax
                                             define-syntax-rule)
                              ((define-syntax . _)
                               (lp l (cons x pre)))
                              ((define-syntax-rule . _)
                               (lp l (cons x pre)))
                              (x
                               (cons (reverse pre) (cons #'x l)))))
                           (()
                            (error "no code in let block"))))))
         (with-syntax (((pre  ...) (car pre.code))
                       ((code ...) (cdr pre.code)))

           #'(let ()
               pre ...
               (list cbegin code ...)))))

      ((<let> (x  ...) code ...)
       (with-syntax (((var ...) (map (lambda (x)
                                       (syntax-case x ()
                                         ((t v i) #'v)
                                         ((v i)   #'v)))
                                     #'(x ...)))
                     ((x2 ...) (map (lambda (x)
                                      (syntax-case x ()
                                        ((t v i)
                                         #`(list t v #,(handle-val #'i)))
                                        ((v i)
                                         #`(list v #,(handle-val #'i)))))
                                    #'(x ...))))
           
         #`(let ((var (gensym "v")) ...)
             (list clet '#,(datum->syntax #'<let>
                                              (source-properties
                                               (syntax->datum xx)))
                   (list x2 ...)
                   (<let> () #,@(arger #'(code ...))))))))))

(define-syntax klet
  (lambda (xx)
    (syntax-case xx ()
      ((klet n lp (x ...) code ...)
       (symbol? (syntax->datum #'lp))
       (with-syntax (((var ...) (map (lambda (x)
                                       (syntax-case x ()
                                         ((t v i) #'v)
                                         ((v i)   #'v)))
                                     #'(x ...)))
                     ((x2 ...) (map (lambda (x)
                                      (syntax-case x ()
                                        ((t v i)
                                         #`(list t v #,(handle-val #'i)))
                                        ((v i)
                                         #`(list v #,(handle-val #'i)))))
                                    #'(x ...))))
         #`(let ((lpn (gensym "lp"))
                 (var (gensym "a"))...)
             (list clet n lpn (list x2 ...)
                   (<let> () 
                          (define-syntax lp
                            (lambda (xx)
                              (syntax-case xx ()
                                ((_ . l)
                                 #`(list lpn '#,(datum->syntax
                                                 #'<let>
                                                 (source-properties
                                                  (syntax->datum xx)))
                                         . l)))))
                          code ...)))))
      
      ((klet n () code ...)
       (let ((pre.code (let lp ((x #'(code ...)) (pre '()))
                         (match x
                           ((x . l)
                            (syntax-case x (define-syntax
                                             define-syntax-rule)
                              ((define-syntax . _)
                               (lp l (cons x pre)))
                              ((define-syntax-rule . _)
                               (lp l (cons x pre)))
                              (x
                               (cons (reverse pre) (cons #'x l)))))
                           (()
                            (error "no code in let block"))))))
         (with-syntax (((pre  ...) (car pre.code))
                       ((code ...) (cdr pre.code)))

           #'(let ()
               pre ...
               (list cbegin code ...)))))

      ((klet n (x  ...) code ...)
       (with-syntax (((var ...) (map (lambda (x)
                                       (syntax-case x ()
                                         ((t v i) #'v)
                                         ((v i)   #'v)))
                                     #'(x ...)))
                     ((x2 ...) (map (lambda (x)
                                      (syntax-case x ()
                                        ((t v i)
                                         #`(list t v #,(handle-val #'i)))
                                        ((v i)
                                         #`(list v #,(handle-val #'i)))))
                                    #'(x ...))))
           
         #`(let ((var (gensym "v")) ...)
             (list clet n (list x2 ...)
                   (<let> () #,@(arger #'(code ...))))))))))

(define (arger0 x)  
  (syntax-case x ()
    ((_ . _)
     x)
    (x
     (handle-val #'x))))

(define (arger x)s  
  (syntax-case x ()
    ((a . l)
     (cons (arger0 #'a) (arger #'l)))
    (a #'a)))


(define-syntax mk
  (syntax-rules ()
    ((_ k nm cnm l ...)
     (begin
       (define-syntax nm
         (lambda (x)
           (syntax-case x ()
             ((_  l ...)
              (let ((n (source-properties (syntax->datum x))))
                (with-syntax (((u (... ...)) (arger (list #'l ...))))
                  #`(list cnm '#,(datum->syntax #'arger n) u (... ...))))))))
       (define-syntax k
         (lambda (x)
           (syntax-case x ()
             ((_ n l ...)
              (with-syntax (((u (... ...)) (arger (list #'l ...))))
                #`(list cnm n u (... ...)))))))))
    
    ((_ k nm cnm . l)
     (begin
       (define-syntax k
         (lambda (x)
           (syntax-case x ()
             ((_  n . l)
              (with-syntax (((u (... ...)) (arger #'l)))
                #`(list cnm n u (... ...)))))))
       
       (define-syntax nm
         (lambda (x)
           (syntax-case x ()
             ((_  . l)
              (let ((n (source-properties (syntax->datum x))))
                (with-syntax (((u (... ...)) (arger #'l)))
                  #`(list cnm '#,(datum->syntax #'arger n)
                          u (... ...))))))))))))

(define-syntax-rule (struct x)  (list cstruct x))
(define-syntax-rule (&   x)      (list cref   x))
(define-syntax-rule (&&  x)      (list cref (list cref   x)))
(define-syntax-rule (&&& x)      (list cref (list cref (list cref x))))


(mk k:      <:>      c: x y)
(mk kif     <if>     cif   p x y)
(mk kcall   call     ccall   . l)
(mk kbegin  <begin>  cbegin2 . l)
(mk k=      <=>      c=      x y)
(mk knot    <not>    cnot    x  )
(mk kbnot   <bnot>   cbnot   x  )
(mk k++     <++>     c++     x  )
(mk k--     <-->     c--     x  )
(mk kderef  <deref>  cderef  x  )
(mk kmember <member> cmember x  y)
(mk k+=     <+=>     c+=     x y)
(mk k-=     <-=>     c-=     x y)
(mk k*=     <*=>     c*=     x y)
(mk kref    <ref>    cref    x  )
(mk k%      <%>      c%      x y)
(mk k!=     <!=>     c!=     x y)

(define (s x)
  (source-properties (syntax->datum x)))

(define-syntax-rule (mkbin k nm cnm default)
  (begin
    (define-syntax k
      (lambda (x)
        (syntax-case x ()
          ((_ n)
           #'default)
          ((_ n  x)
           (arger0 #'x))
          ((_ n a b)
           #`(list cnm n #,(arger0 #'a) #,(arger0 #'b)))
          ((_ n a . l)
           #`(list cnm n a (k n . l))))))

    (define-syntax nm
      (lambda (x)
        (syntax-case x ()
          ((_)
           #'default)
          ((_  x)
           (arger0 #'x))
          ((_ a b)
           (let ((n (s x)))
             #`(list cnm '#,(datum->syntax #'arger n)
                     #,(arger0 #'a) #,(arger0 #'b))))
          ((_ a . l)
           (let ((n (s x)))
             (with-syntax ((rest #`(k '#,(datum->syntax #'arger n) . l)))
               #`(list cnm '#,(datum->syntax #'touch n)
                       #,(arger0 #'a) rest)))))))))

(mkbin kand  <and>  cand  ctrue)
(mkbin kor   <or>   cor   cfalse)
(mkbin kband <band> cband (<bnot> 0))
(mkbin kbor  <bor>  cbor  0)
(mkbin kxor  <bxor> cbxor 0)
(mkbin k+    <+>    c+    0)
(mkbin k*    <*>    c*    1)    
(mkbin k/    </>    c/    'error)
(define-syntax-rule (mkbin2 k nm cnm)
  (begin
    (define-syntax k
      (lambda (x)
        (syntax-case x ()
          ((_ n a b)
           #`(list cnm n #,(arger0 #'a) #,(arger0 #'b)))
          ((_ n a b . l)
           #'(<and> (k n a b) (k n b . l))))))
    
    (define-syntax nm
      (lambda (x)
        (syntax-case x ()
          ((_ a b)
           (let ((n (source-properties (syntax->datum x))))
             #`(list cnm '#,(datum->syntax #'arger n)
                     #,(arger0 #'a) #,(arger0 #'b))))
          ((_ a b . l)
           (with-syntax ((nn #`'#,(datum->syntax #'arger
                                                 (source-properties
                                                  (syntax->datum x)))))
             #'(<and> (k nn a b) (k nn b . l)))))))))

(mkbin2 k<  q<  c<)
(mkbin2 k>  q>  c>)
(mkbin2 k<= q<= c<=)
(mkbin2 k>= q>= c>=)
(mkbin2 k== q== c==)

(define-syntax <c>
  (syntax-rules ()
    ((_ type x)
     (list c: type (<c> x)))
    ((_ #t) (list c ctrue))
    ((_ #f) (list c cfalse))
    ((_ x ) (list c x))))

(define prefix "")

(define (tname nm)
  (string->symbol
   (list->string
    (let lp ((l (append
                 (string->list prefix)
                 (string->list (symbol->string nm)))))
      (if (pair? l)
          (cond
           ((eq? (car l) #\-)
            (cons* #\_ #\_ (lp (cdr l))))
           ((eq? (car l) #\?)
            (cons* #\_ #\p #\_ (lp (cdr l))))
           ((eq? (car l) #\!)
            (cons* #\_ #\e #\_ (lp (cdr l))))
           (else
            (cons (car l) (lp (cdr l)))))
          '())))))
                      
(define (trname nm)
  (datum->syntax
   #'tname
   (tname (syntax->datum nm))))

(define compiler (make-fluid #f))
(define-syntax-rule (const x)  (list cconst x))
(define-syntax-rule (fkn . l)  (list cfkn . l))

(define-syntax c-define 
  (lambda (x)
    (syntax-case x ()
      ((_ m rtype name ((type var) ...) code ...)
       (with-syntax ((cnm (trname #'name)))
         #'(begin
             (define-syntax name
               (lambda (x)
                 (syntax-case x ()
                   ((_ . l)
                    (with-syntax ((n (datum->syntax
                                      #'arger
                                      (source-properties
                                       (syntax->datum x)))))
                      #`(list name 'n #,@(arger #'l))))
                   (_
                    #`(list cfkn 'cnm (list rtype type ...))))))
             
             (write port
               ((fluid-ref compiler)
                (let m
                    (list cdefine rtype 'cnm (list (list type var) ...)
                          (<begin> code ...)))))))))))


(define-syntax c-declare
  (lambda (x)
    (syntax-case x ()
      ((_ rtype name ((type var) ...) code ...)
       (with-syntax ((cnm (trname #'name)))
         #'(begin
             (define-syntax name
               (lambda (x)
                 (syntax-case x ()
                   ((_ l (... ...))
                    #`(kcall '#,(datum->syntax
                                 #'arger
                                 (source-properties
                                  (syntax->datum x)))
                             (list rtype type ...)
                             l (... ...))))))))))))

(define-syntax-rule (c-struct name (type id) ...)
  (begin
    (define name (kind-ground cstruct name))
    (set-procedure-property! name 'cname (tname 'name))
    (let ((k (list (list 'id type) ...)))
      (write port (list cstruct name k))
      (add-struct name k))))

(define-syntax k->
  (lambda (x)
    (syntax-case x ()
      ((_ n a)
       #'a)
      ((_ n a b)
       (let ((bb (syntax->datum #'b)))
         (cond
          ((and (number? bb) (integer? bb))
           #'(kref n a b))
          ((eq? bb '*)
           #'(kderef n a))
          (else
           #'(kmember n a b)))))
      ((_ n a b . l)
       #'(k-> n (k-> n a b) . l)))))
              

(define-syntax <->>
  (lambda (x)
    (syntax-case x ()
      ((_ a)
       #'a)
      ((_ a b)
       (let ((bb (syntax->datum #'b))
             (n  (datum->syntax
                  #'k->
                  (source-properties
                   (syntax->datum x)))))

                                
         (cond
          ((and (number? bb) (integer? bb))
           #`(kref '#,n a b))
          ((eq? bb '*)
           #`(kderef '#,n a))
          (else
           #`(kmember '#,n a b)))))
      ((_ a b . l)
       (let ((n  (datum->syntax
                  #'k->
                  (source-properties
                   (syntax->datum x)))))
         
         #`(k-> '#,n (k-> '#,n a b) . l))))))

(define-syntax-rule (with-new-symbols ((nm cnm) ...) code ...)
  (let ()
    (define-syntax cnm
      (lambda (x)
        (syntax-case x ()
          ((_ . l)
           #`(nm '#,(datum->syntax #'arget
                                   (source-properties
                                    (syntax->datum x))) . l)))))
    ...
    code ...))

(define-syntax-rule (mk-language with-c-symbols (nm cnm) ...)
   (define-syntax with-c-symbols
     (lambda (x)
       (syntax-case x ()
         ((_ stx code (... ...))
          (let ()
            (define (f nmx) (datum->syntax #'stx nmx))
            (with-syntax ((cnm (f 'cnm)) ...)
              #'(with-new-symbols ((nm cnm) ...) code (... ...)))))))))

(mk-language with-c-symbols
   (k++     ++    )
   (k--     --    )          
   (k:      :     )
   (kif     if    )
   (klet    let   )
   (kbegin  begin )
   (k->     ->    )
   (k<      <     )
   (k<=     <=    )
   (k>      >     )
   (k>=     >=    )
   (k+      +     )
   (k-      -     )
   (k*      *     )
   (k/      /     )
   (k=      =     )
   (kand    and   )
   (kor     or    )
   (knot    not   )
   (k+=     +=    )
   (k-=     -=    )
   (k*=     *=    )
   (k==     ==    )
   (k!=     !=    )
   (kband   logand)
   (kbor    logior)
   (kbxor   logxor)
   (kbnot   lognot))

#|
 (test
  (let lp ((i 0) (s 0))
    (if (< i 100)
        (lp (+ i 1) (+ s i 2))
        s)))

 (test
  (let lp ((i 0h) (s 0h))
    (if (< i 100h)
        (lp (+ i 1h) (+ s i 2h))
        s)))

 (test
  (let lp ((i 0) (s 0))
    (if (< i 100)
        (lp (+ i 1) (+ s i 2UL))
        s)))

(test
  (let lp ((i 0) (s 0))
    (if (< i 100)
        (begin
          (lp (+ i 1) (+ s i 2UL))
           s)
        s)))


|#
