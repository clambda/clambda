(define-module (clambda compile)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 match)
  #:use-module ((logic guile-log) #:select (<define> <var> <=> <lookup>
                                                     <scm> <case-lambda>))
  #:use-module (clambda types) 
  #:use-module ((clambda ctypes))
  #:use-module ((clambda macros) #:select (with-c-symbols))
  #:use-module ((clambda ctypes) #:select (inttypes numtypes))
  #:use-module (clambda clang)
  #:use-module (logic guile-log prolog swi)
  #:use-module ((logic guile-log iso-prolog) #:renamer
                (lambda (s)
                  (if (eq? s 'float)
                      'iso:float
                      s)))
  #:export (test1 test))


(compile-prolog-string
"
init :-
   b_setval(tags,[0]).
")


(define generate_id
  (<case-lambda>
   ((x n)
    (<var> (t)
      (b_getval tags t)
      (let* ((l (<lookup> t))
             (i (car l))
             (d (cdr l))
             (m (cons (+ i 1) d)))
        (<=> x ,(string->symbol (format #f "~a~{_~a~}" n m)))
        (b_setval tags m))))
   ((x)
    (generate_id x 'x))))

(<define> (push)
   (<var> (v)
     (b_getval tags v)
     (b_setval tags (cons 0 (<lookup> v)))))     

(<define> (pop)
   (<var> (v)
     (b_getval tags v)
     (b_setval tags (cdr (<lookup> v)))))

(compile-prolog-string
"
is_const_type(T) :- lookupGround(T,[cconst,_]).
fkn_type(T)      :- lookupGround(T,[cfkn|_]).

check(X,Y) :- (var(X);var(Y)),!,X=Y.

check([X|L],[XX|LL]) :- !,
  check(X,XX),
  check(L,LL).

check(X,X) :- !.
check(X,Y) :-
  lookupGround(X,XX),
  lookupGround(Y,YY),
  (
    atom(XX) ->
       XX=YY;
    check(XX,YY)
  ).

-trace.
remove_const(T,TT) :-
   var(T) ->
      T=TT;
   (
     lookupGround(T,T1),
     (
       T1 = [cconst,T2] ->
          remove_const(T2,TT);
       check(T,TT)
     )
   ).

x_types(F,T0,T1,T2):-
  (var(T0) -> T=T0 ; remove_const(T0,T)),
  remove_const(T1,T),
  remove_const(T2,T),
  F(T).

x_types(F,T0,T1):-
  (var(T0) -> T=T0 ; remove_const(T0,T)),
  remove_const(T1,T),
  F(T).

num_types(A,B,C) :- x_types(num_types,A,B,C).   
num_types(A,B)   :- x_types(num_types,A,B).   

num_types(T) :- 
  lookupGround(T,TT),
  member(TT,numtypes).

int_types(A,B,C) :- x_types(int_types,A,B,C).   
int_types(A,B)   :- x_types(int_types,A,B).   

int_types(T) :- 
  lookupGround(T,TT),
  member(TT,inttypes).

bit_types(A,B,C) :- x_types(bit_types,A,B,C).   
bit_types(A,B)   :- x_types(bit_types,A,B).   

bit_types(T) :- 
  lookupGround(T,TT),
  member(TT,bittypes).
")


(compile-prolog-string
"
compile(X,R) :-
  compile(_,X,R).

compile(T,X,R) :-
  init,
  catch(compile0([T,ret] : X,R,[],[#t]),E,
      (write(compile_error(E)),nl,fail)).

initial_bindings([],[],[]).

initial_bindings([[T,V]|L],[(T : V -> Vq)|X],[[T,Vq]|Y]) :-
   generate_id(Vq),
   initial_bindings(L,X,Y).

-trace.
-extended.
compile0([T,R] : [cconst,X], [cbegin,['c:',TT,V],Z|YY], Tr, _) :-
   generate_id(V),
   (var(R) -> YY=[] ; YY = [['c=',R,V]]),   
   compile0([TT,V] : X, Z, Tr, []),
   (
      is_const_type(T) ->
        check(T,TT);
      check(T,[cconst,TT])
   ).

compile0(_ : [cdefine,T,NM,Args,Code],
         [cdefine,T,NM,AArgs,[cbegin,['c:',T,Ret],XX,[creturn,Ret]]],
         _,_) :- !,
   generate_id(Ret),
   initial_bindings(Args,Tr,AArgs),
   compile0([T,Ret] : Code, XX, Tr, [#t]).

compile0([T,R] : ['c=',N,A,B],
            [cbegin,
               ['c:',T,VB],AA,BB,
               E], Tr, Tail) :- !,
    generate_id(VB),
    F=['c=',LA,VB],
    ( 
      compile([S,VB] : B, BB, Tr, #f) ->
        true;
      throw(type_error('=',T,N))
    ),

    remove_const(S,T),
    
    (
       fkn_type(T) ->
          throw(type_error('=',N,T,'pure function type did you miss &f'));
       true
    ),

    (
       lvalue(T : A, AA, Tr, AA) ->
          true;
       throw(lvalue_error('=',T,N))
    ),
    (var(R) -> E=F ; E=['c=',R,F]).

compile0([T,R] : [(Op,('c+=';'c-=';'c/=';'c*=')),N,A,B],
            [cbegin,
               ['c:',T,VB],
               AA,BB,E], Tr, Tail) :- !,   
    generate_id(VB),
    F=[Op,LA,VB],
    (
      compile([S,VB] : B, BB, Tr, #f) ->
         true;
      throw(type_error(OP,T,N))
    ),
    remove_const(S,T),
    (
      lvalue(T : A, AA, Tr, AA) ->
         true;
      throw(lvalue_error(OP,T,N))
    ),
    (var(R) -> E=F ; E=['c=',R,F]),
    (
       num_types(T) ->
         true;
       throw(type_error(numeric,T,N))
    ).

compile0([T,R] : [(Op,('c++';'c--')),N,A],
            [cbegin,
               [cbegin|AA],
               E], Tr, Tail) :- !,
    F=[Op,LA],
    (
       lvalue(T : A, AA, Tr, AA) ->
         true;
       throw(lvalue_error(OP,T,A,N))
    ),
    no_const(T),
    (var(R) -> E=F ; E=['c=',R,F]),
    (
      num_types(T) ->
         true;
      throw(type_error(numeric,Op,T,N))
    ).

compile0([T,R] : ['c%=',N,A,B],
            [cbegin,
               ['c:',T,VB],AA,BB,
               E], Tr, Tail) :- !,
    generate_id(VB),
    F=['c%=',LA,VB],
    (
      compile([S,VB] : B, BB, Tr, #f) ->
         true;
      throw(type_error('%',T,N))
    ),
    remove_const(S,T),
    (  
       lvalue(T1 : A, AA, Tr, AA) ->
         true;
       throw(lvalue_error(OP,T,N))
    ),
    (var(R) -> E=F ; E=['c=',R,F]),
    (
      int_types(T) ->
        true;
      throw(type_error(integer,'%',T,N))
    ).


compile0([T,R] : [(Op,('c+';'c-';'c/';'c*')),N,A,B],
         [cbegin,
           ['c:',T,VA],
           ['c:',T,VB],
           AA,BB,E], Tr, Tail) :- !,
   generate_id(VA),
   generate_id(VB),
   F=[Op,VA,VB],
   (var(R) -> E=F ; E=['c=',R,F]),
   (
      compile0([T1,VA] : A, AA, Tr, #f) ->
         true;
      throw(type_error(OP,arg1,T,N))
   ),
   (
     compile0([T2,VB] : B, BB, Tr, #f) ->
       true;
     throw(type_error(Op,arg2,T,N))
   ),
   (
     num_types(T,T1,T2) ->
        true;
     throw(type_error(number,Op,T,N))
   ).

compile0([T,R] : [(Op,('c%')),N,A,B],
         [cbegin,
           ['c:',T,VA],
           ['c:',T,VB],
           AA,BB,E], Tr, Tail) :- !,
   generate_id(VA),
   generate_id(VB),
   F=[Op,VA,VB],
   (var(R) -> E=F ; E=['c=',R,F]),
   (
      compile0([T1,VA] : A, AA, Tr, #f) ->
         true;
      throw(type_error(OP,arg1,T,N))
   ),
   (
     compile0([T2,VB] : B, BB, Tr, #f) ->
       true;
     throw(type_error(Op,arg2,T,N))
   ),
   (
     int_types(T,T1,T2) ->
        true;
     throw(type_error(number,Op,T,N))
   ).

compile0([T,R] : [(Op,('cband';'cbor';'cxor')),N,A,B],
         [cbegin,
           ['c:',T,VA],
           ['c:',T,VB],AA,BB,
           E], Tr, Tail) :- !,
   generate_id(VA),
   generate_id(VB),
   F=[Op,VA,VB],
   (var(R) -> E=F ; E=['c=',R,F]),
   (
      compile0([T1,VA] : A, AA, Tr, #f) ->
         true;
      throw(type_error(OP,arg1,T,N))
   ),
   (
     compile0([T2,VB] : B, BB, Tr, #f) ->
       true;
     throw(type_error(Op,arg2,T,N))
   ),
   (
     bit_types(T,T1,T2) ->
        true;
     throw(type_error(bittype,Op,arg2,T,N))
   ).

compile0([bool,R] : [(Op,('c<';'c>';'c<=';'c>=')),N,A,B],
         [cbegin,
           ['c:',T,VA],
           ['c:',T,VB],AA,BB,
           E], Tr, Tail) :- !,
   generate_id(VA),
   generate_id(VB),
   F=[Op,VA,VB],
   (var(R) -> E=F ; E=['c=',R,F]),
   (
      compile0([T1,VA] : A, AA, Tr, #f) ->
         true;
      throw(type_error(OP,arg1,T,N))
   ),
   (
     compile0([T2,VB] : B, BB, Tr, #f) ->
       true;
     throw(type_error(Op,arg2,T,N))
   ),
   (
     num_types(_, T1, T2) ->
        true;
     throw(type_error(number,Op,T,N))
   ).


compile0([bool,R] : [(Op,('cand';'cor')),N,A,B],
         [cbegin,
           ['c:',bool,VA],
           ['c:',bool,VB],AA,BB,
           E], Tr, Tail) :- !,
   generate_id(VA),
   generate_id(VB),
   F=[Op,VA,VB],
   (var(R) -> E=F ; E=['c=',R,F]),
   ( 
      compile0([bool,VA] : A, AA, Tr, #f) ->
         true;
      throw(type_error(OP,arg1,bool,N))
   ),
   (
     compile0([bool,VB] : B, BB, Tr, #f) ->
       true;
     throw(type_error(Op,arg2,bool,N))
   ).

compile0([bool,R] : [(Op,'cnot'),A],
         [cbegin,
           ['c:',bool,VA],AA,
           E], Tr, Tail) :- !,
   generate_id(VA),
   F=[Op,VA],
   (var(R) -> E=F ; E=['c=',R,F]),
   (
      compile0([bool,VA] : A, AA, Tr, #f) ->
         true;
      throw(type_error(OP,bool,N))
   ).


compile0([bool,R] : [(Op,('c==';'c!=')),N,A,B],
         [cbegin,
           ['c:',T,VA],
           ['c:',T,VB],AA,BB,
           E], Tr, Tail) :- !,
   generate_id(VA),
   generate_id(VB),
   F=[Op,VA,VB],
   (var(R) -> E=F ; E=['c=',R,F]),
   (
      compile0([T,VA] : A, AA, Tr, #f) ->
         true;
      throw(type_error(OP,arg1,T,N))
   ),
   (
     compile0([T,VB] : B, BB, Tr, #f) ->
       true;
     throw(type_error(Op,arg2,T,N))
   ).

compile0([T,R] : [(Op,('c+';'c-')),N,A],
         [cbegin,
           ['c:',T,VA],AA,
           E], Tr, Tail) :- !,
   generate_id(VA),
   F=[Op,VA],
   (var(R) -> E=F ; E=['c=',R,F]),
   (
      compile0([T1,VA] : A, AA, Tr, #f) ->
         true;
      throw(type_error(OP,arg1,T,N))
   ),
   (
     num_types(T,T1) ->
        true;
     throw(type_error(number,Op,T,N))
   ).


compile0([T,R] : [(Op,cbnot),N,A],
         [cbegin,
           ['c:',T,VA],AA,
           E], Tr, Tail) :- !,
   generate_id(VA),
   F=[Op,VA],
   (var(R) -> E=F ; E=['c=',R,F]),
   (
      compile0([T1,VA] : A, AA, Tr, #f) ->
         true;
      throw(type_error(OP,arg1,T,N))
   ),
   (
     bit_types(T,T1) ->
        true;
     throw(type_error(bittype,Op,T,N))
   ).

compile0([T,R] : [cif,N,P,X,Y],[cbegin,['c:',int,B],PP,[cif,B,XX,YY]],
         Tr,Tail) :- !,
    generate_id(B),
    (
      compile0([bool  ,B] : P, PP, Tr,#f) -> 
         true;
      throw(type_error(cif,condition,bool,N))
    ),
    (
       compile0([T     ,R] : X, XX, Tr,Tail) ->
           true;
       throw(type_error(cif,branch1,T,N))
    ),
    (
       compile0([T     ,R] : Y, YY, Tr,Tail) ->
          true;
       throw(type_error(cif,branch2,T,N))
    ).


compile0(T : [cbegin|L],[cbegin|LL], Tr, Tail) :- !,
    compile_begin(T,L,LL,Tr,Tail).

compile0(T : [cbegin2,N|L],[cbegin|LL], Tr, Tail) :- !,
    compile_begin(T,L,LL,Tr,Tail).


compile0(T : [clet,N,Specs|Code], [cbegin|L], Tr, Tail) :-
    (Specs=[];Specs=[_|_]),
    !,
    (
      compile_spec(Specs,U,Tr,TTr) ->
         true;
      throw(spec_error(clet,N))
    ),
    append(U,[BL],L),
    (
       compile0(T:[cbegin|Code],BL,TTr,Tail) ->
           true;
       throw(type_error(clet,T,N))
    ).

compile0(T : [clet,N,Lam,Specs|Code], [cbegin|L], Tr, Tail) :- !,
    generate_id(V,\"lp\"),   
    (
       compile_spec(Specs,U,Tr,TTr,TT,VV) ->
          true;
       throw(spec_error(clet,N))
    ),
    TTTr = [[[loop,TT,VV] : Lam -> V]|TTr],
    append([U,[[label,V]],[BL]],L),
    TTail = [Lam|Tail],
    (
       compile0(T:[cbegin|Code],BL,TTTr,TTail) ->
         true;
       throw(type_error(clet,T,N))
    ).

compile0([T,R] : ['c:',N,T2,X], Y, Tr, Tail) :- !,
  (
    number(X) ->
      (
         when[(validate (<lookup> T2) (<lookup> X))] ->
            (
               T=T2,
               Y=['c=',R,[ccast,T2,X]]
            );
         throw(validate_error(T2,X))
      );
    (
      generate_id(V),
      Y = [cbegin,['c:',T3,V],Z|YY],
      T=T2,
      compile0([T3,V] : X, Z, Tr, Tail),
      (var(R) -> YY=[] ; YY = [['c=',R,[ccast,T2,V]]]),
      (
        derive_type(T2,T3) ->
           true;
        throw(derive_error(T3,T2))
      )
    )
  ).

compile0([T,R] : [cmember,N,X,I],[cbegin,['c:',TT,V],XX|It],Tr,_) :- !,
   generate_id(V),
   compile0([TT,V] : X, XX, Tr, #f),
   derefify(TT,TTT,V,VV),
   struct_lookup(TTT,I,TT),
   check(T,TT),
   (var(R) -> It=[] ; It = [['c=',R,[cmember,VV,I]]]).


compile0([T,R] : [cderef,N,X],[cbegin,['c:',TT,V],XX|It],Tr,_) :- !,
   generate_id(V),
   compile0([TT,V] : X, XX, Tr, #f),
   (
      lookupGround(TT,[cref,T]) -> 
         true; 
      throw(type_error('*',N,T,TT))
   ),
   (var(R) -> It=[] ; It = [['c=',R,[cderef,V]]]).


compile0([T,R] : [cref,X],[cbegin,['c:',TT,V],XX|It],Tr,_) :- !,
   generate_id(V),
   compile0([TT,V] : X, XX, Tr, #f),
   (
      lookupGround(T,[cref,TT]) -> 
         true; 
      throw(type_error('&',T,TT))
   ),
   (var(R) -> It=[] ; It = [['c=',R,[cref,V]]]).


compile0([T,R] : [c,A],Y,Tr,Tail) :- !,
   compile0([T,R] : A,Y,Tr,Tail).

compile0([[cfkn|D],R] : [cfkn,Nm,D],Def,_,_) :-
   var(R) -> Def = [cbegin] ; Def = ['c=',R,Nm].

compile0([T,R] : [ccall,N,F|Args],[cbegin,[cbegin|ArgsDefs],Call],
              TTr,Tail) :- !,
  compile_args([[cref,[cfkn,T|ArgsTypes]]|ArgsTypes],[F|Args],ArgsIt,
                   ArgsDefs1,ArgsCode,TTr),
  append(ArgsDefs1,ArgsCode,ArgsDefs),
  (
    var(R) -> 
       Call = ['c=',R,[ccall | ArgsIt]];
    Call = [ccall | ArgsIt]
  ).

compile0([T,R] : [F,N|Args],[cbegin,[cbegin|ArgsDefs],Call],
              TTr,Tail) :- !,
  (  
     member([[loop,Ts,Ls] : F -> Vs],TTr) ->
     (
       (member(F,Tail) -> true ; throw(recur_at_nontail(F,N))),
       compile_recur(Args,Ts,Ls,ArgsDefs,TTr),
       Call = [goto,Vs]
     );
    (
      (
           compile_args([[cfkn,T|ArgsTypes]|ArgsTypes],[F|Args],ArgsIt,
                         ArgsDefs1,ArgsCode,TTr) ->
               true;
           throw(type_error(fkn,N))
      ),
      append(ArgsDefs1,ArgsCode,ArgsDefs),
      (
         var(R) -> 
            Call = ['c=',R,[ccall | ArgsIt]];
         Call = [ccall | ArgsIt]
      )
    )
  ).
   
compile0([T,R] : X,Y,Tr,Tail) :- !,
  (
    var(R)  -> 
        Y=[cbegin];
    (X=ctrue;X=cfalse) ->
    (        
        groundType(X,T),
        Y=['c=',R,X]
    );
    when[(symbol? (<lookup> X))] ->
        (               
            member((TT:X->Xq),Tr) ->
               (
                  remove_const(TT,T),
                  Y = ['c=',R,Xq]
               );               
            throw(error('variable declaration missing',X))
        );
    (        
        groundType(X,T),
        Y=['c=',R,X]
    )
  ).
")


(compile-prolog-string
"
compile_recur([],[],[],[],_).
compile_recur([A|Args],[T|Ts],[V|Vs],[Def|Defs],Tr) :-
   compile0([TT,V] : A, Def, Tr, #f),
   check(TT,T),
   compile_recur(Args,Ts,Vs,Defs,Tr).   
")

(compile-prolog-string
"
compile_args([],[],[],[],[],_).
compile_args([T|Ts],[Exp|Exps],[V|Vs],[['c:',TT,V]|Defs],[Code|Codes],Tr) :- !,
    generate_id(V),
    compile0([TT,V] : Exp, Code , Tr, #f),
    remove_const(T,TT),
    compile_args(Ts,Exps,Vs,Defs,Codes,Tr).
")


(compile-prolog-string
"
groundType(X,T) :-
  (X=ctrue;X=cfalse) -> 
     T=bool;
  integer(X) ->
     T=long;
  float(X) ->
     T=scm[(@@ (clambda ctypes) double)].
")


(compile-prolog-string
"
compile_begin([T,R],[],XX,_) :- !,
  (
    var(R) -> 
      XX=[cbegin]; 
    XX=['c=',R,unspecified]
  ).

compile_begin(T,[X],[XX],Tr,Tail) :- !,
    compile0(T : X,XX,Tr,Tail).

compile_begin(T,[X|L],[XX|LL],Tr,Tail) :-
    compile0(_ : X,XX,Tr,#f),
    compile_begin(T,L,LL,Tr,Tail).
")


(compile-prolog-string
 "
lvalue(T : [cref,X],[cref,XX],Tr,E) :- 
   lvalue([cderef,T] : X, XX,Tr, E).
lvalue(T : [cvec,X,I],[cvec,XX,II],Tr,[[cbegin|EE]|E]) :-
   generate_id(II),
   compile([S,II] : I, EE, Tr, #f),
   lvalue([cref,T] : X, XX, Tr, E).
lvalue(T : [cmember,X,V],[cmember,XX,V],Tr,E) :-
   lvalue(S : X, XX, Tr, E),
   struct_lookup(S,V,T).
lvalue(T : X, X, Tr, []) :-
   member([T:X],Tr).

struct_lookup(S,V,T) :-
   lookupGround(S,SS),
   struct_lookup0(SS,V,T).

struct_lookup0([cstruct,N],V,T) :-
   X=scm[(pk 'lkp (lookup-cstruct (<scm> N) (<scm> V)))],
   (
     X=#f ->
        fail; 
     X=[_,T]
   ).

lookupGround(S,SS) :-
   SSS = scm[(get-ground (<scm> S))],
   (SSS == #f -> check(SS,S) ; check(SS,SSS)).
")


(compile-prolog-string
" 
compile_spec([],[],Tr,Tr).

compile_spec([[T,V,Val]|L],[['c:',T,Vq],Set|LL],Tr,TTr) :-
    generate_id(Vq),
    compile0([T,Vq] : Val,Set,Tr,#f),
    Tr2 = [(T : V -> Vq)|Tr],
    compile_spec(L,LL,Tr2,TTr).

compile_spec([[V,Val]|L],[['c:',T,Vq],Set|LL],Tr,TTr) :-
    generate_id(Vq),
    compile0([T,Vq] : Val,Set,Tr,#f),
    Tr2 = [(T : V -> Vq)|Tr],
    compile_spec(L,LL,Tr2,TTr).

compile_spec([],[],Tr,Tr,[],[]).

compile_spec([[T,V,Val]|L],[['c:',T,Vq],Set|LL],Tr,TTr,[T|TT],[Vq|VV]) :-
    generate_id(Vq),
    compile0([T,Vq] : Val,Set,Tr,#f),
    Tr2 = [(T : V -> Vq)|Tr],
    compile_spec(L,LL,Tr2,TTr,TT,VV).
compile_spec([[V,Val]|L],[['c:',T,Vq],Set|LL],Tr,TTr,[T|TT],[Vq|VV]) :-
    generate_id(Vq),
    compile0([T,Vq] : Val,Set,Tr,#f),
    Tr2 = [(T : V -> Vq)|Tr],
    compile_spec(L,LL,Tr2,TTr,TT,VV).
")

(compile-prolog-string
"
-trace.
-extended.
derefify((T,[cstruct,_]),T,V,V) :- !.

derefify([cref,TT],TTT,V,VV) :- !,
  V=[cref,V2],
  derefify(TT,TTT,V2,VV).

derefify(T,TT,V,VV) :-
  lookupGround(T,TTT),
  (
     (TTT = [cstruct,_] ; TTT = [cref,_]) ->
         derefify(TTT,TT,V,VV);
     throw(type_error(cmember,T))
  ).
")

(compile-prolog-string
"
derive_type(A,B) :-
   lookupGround(A,AA),
   lookupGround(B,BB),
   derive(AA,BB).

derive(X,X).
derive([cconst,T],T).
derive([cref,_],[cref,_]).
derive(long,int).
derive(long,short).
derive(long,char).
derive(int,short).
derive(int,char).
derive(short,char).
derive(long,uint).
derive(long,ushort).
derive(long,uchar).
derive(int,ushort).
derive(int,uchar).
derive(short,uchar).
derive(ulong,uint).
derive(ulong,ushort).
derive(ulong,uchar).
derive(uint,ushort).
derive(uint,uchar).
derive(ushort,uchar).
derive(double,float).
derive(X,bool) :- int_types(X).
")

(define-syntax test
  (lambda (x)
    (syntax-case x ()
      ((_ code)
       #`(pretty-print
          (tr
           (prolog-run 1 (t r)
            (compile t (with-c-symbols #,x code) r))))))))

(define (compiler x)
  (let ((r (prolog-run 1 (r) (compile x r))))
    (if (pair? r)
        (let ((r (car r)))
          (pretty-print (tr r))
          r))))

(define (tr x)
  (let lp ((x x))
    (match x
      ((x . l)
       (cons (lp x) (lp l)))
      (x
       (if (procedure? x)
           (procedure-name x)
           x)))))

       
#|
(define (f x y z)
|#

