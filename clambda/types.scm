(define-module (clambda types)
  #:use-module (logic guile-log iso-prolog)
  #:export (typedef derive ground define-c-types add-validator validate
                    add-struct lookup-cstruct kind-ground
                    get-ground))

(define types (make-hash-table))

(eval-when (load eval compile)
  (compile-prolog-string "
     mk(X,A) :- A=X@@!, write(A),nl.
   "))

(eval-when (load eval compile)
  (define (make-type symbol)
    (car
     (prolog-run 1 (t)
      (mk (symbol->string symbol) t)))))

(define-syntax-rule (typedef from to)
  (define to
    (let ((t (make-type 'to)))
      (hash-set! types t from)
      t)))

(define (derive a b)
  (if (equal? a b)
      #t
      (let ((new (hash-ref types a)))
        (if new
            (derive new b)
            #f))))

(define (get-ground a)
  (let ((new (hash-ref types a)))
    (if new
        (if (eq? new #t)
            a
            (get-ground new))
        a)))
  

(define validators (make-hash-table))
(define-syntax-rule (ground nm)
  (let ((nm (make-type 'nm)))
    (hash-set! validators nm (lambda (x) #f))
    (hash-set! types nm #t)
    nm))

(define-syntax-rule (kind-ground kind nm)
  (let ((nm (make-type 'nm)))
    (hash-set! validators nm (lambda (x) #f))
    (hash-set! types (list kind nm) #t)
    nm))

(define (add-validator x f)
  (hash-set! validators x f))

(define-syntax-rule (def define-c-types a ...)
  (define-syntax define-c-types
    (lambda (x)
      (syntax-case x ()
        ((q)
         (with-syntax ((a (datum->syntax #'q 'a)) ...)
           #'(begin (ground a) ...)))))))

(define (validate type x) ((hash-ref validators type) x))

(define structs (make-hash-table))
(define (add-struct nm data)
  (hash-set! structs nm data))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))
(define (lookup-cstruct nm v)
  (aif it (hash-ref structs nm #f)
       (assoc v it)
       #f))

(define functions '())

(def define-c-types bool char short int long uchar ushort uint ulong float double)

